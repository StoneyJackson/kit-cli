#!/usr/bin/env bash
BIN_DIR="$(cd -- "$(dirname -- "${BASH_SOURCE[0]:-$0}";)" &>/dev/null && pwd 2>/dev/null;)";

IMAGE_NAME="${PIPELINE_IMAGE_NAME:-kit}"

cd "$BIN_DIR/.." || exit
bin/build.bash --target test
docker run \
    --rm \
    --env-file .env \
    --workdir /app \
    --entrypoint "shellspec" \
    "$IMAGE_NAME" "$@"
