#!/usr/bin/env bash
BIN_DIR="$(cd -- "$(dirname -- "${BASH_SOURCE[0]:-$0}";)" &>/dev/null && pwd 2>/dev/null;)";

shellcheck() {
    docker run --rm -v "$PWD:/mnt" \
        koalaman/shellcheck:stable -C --shell=bash -x "$@"
}

markdownlint_cli2() {
    docker run -v "${PWD}:/w" -w /w \
        registry.gitlab.com/pipeline-components/markdownlint-cli2:latest \
        markdownlint-cli2 "$@"
}

cd "$BIN_DIR/.." || exit

# shellcheck disable=SC2046
markdownlint_cli2 '**/*.md' '#.git'
shellcheck bin/*.bash spec/*.sh src/bin/* src/lib/*
