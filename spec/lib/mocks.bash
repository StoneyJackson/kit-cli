GH_TOKEN=bogus-gh-token
GITLAB_TOKEN=bogus-gitlab-token

export MOCKED_TRACE=()

export MOCKED_GIT_TRACE=()
git() {
    MOCKED_TRACE+=("git $*")
    MOCKED_GIT_TRACE+=("git $*")
    is-push() (
            set +u
            while [ -n "$1" ] ; do
                if [ "$1" == "push" ] ; then
                    exit 0
                fi
                shift
            done
            exit 1
    )

    if is-push "$@" ; then
        echo "MOCKED: git $*"
    else
        \command git "$@" &> /dev/null
    fi
}

export MOCKED_GH_TRACE=()
gh() {
    MOCKED_TRACE+=("gh $*")
    MOCKED_GH_TRACE+=("gh $*")
}

export MOCKED_GLAB_TRACE=()
glab() {
    MOCKED_TRACE+=("glab $*")
    MOCKED_GLAB_TRACE+=("glab $*")
}

export MOCKED_CURL_TRACE=()
curl() {
    MOCKED_TRACE+=("curl $*")
    MOCKED_CURL_TRACE+=("curl $*")
}

reset_mocked_traces() {
    MOCKED_CURL_TRACE=()
    MOCKED_GH_TRACE=()
    MOCKED_GIT_TRACE=()
    MOCKED_GLAB_TRACE=()
    MOCKED_TRACE=()
}
