GITLAB_TEST_PROJECT="${GITLAB_TEST_GROUP}/capture-test"
GITHUB_TEST_PROJECT="${GITHUB_TEST_GROUP}/capture-test"

Describe "kit capture"

    BeforeEach tempdir_enter
    AfterEach tempdir_exit

    Context "kit capture <url>"
        Parameters
            "${GITLAB_TEST_PROJECT}"
            "${GITHUB_TEST_PROJECT}"
        End

        delete_kit_project() {
            rm -rf .kit/artifacts/capture-test
        }
        AfterEach 'delete_kit_project'

        It "creates the local project by the same name at the top-level."
            When call _kit capture "${1}"
            The file .kit/artifacts/capture-test/project should exist
        End
    End

    Context "kit capture <url> <pathname>"
        It "Creates .kit/artifacts/<pathname>/project"
            When call _kit capture "${GITLAB_TEST_PROJECT}" a/b/c
            The file .kit/artifacts/a/b/c/project should exist
        End
    End

    Context ".kit exists in ancestor"
        It "uses the .kit in the ancestor directory."
            make_paths g/.kit g/p/k
            pushd_ g/p/k
            When call _kit capture "${GITLAB_TEST_PROJECT}" a/b/c
            popd_
            The file g/.kit/artifacts/a/b/c/project should exist
        End
    End

    Context "kit capture repo <url>"
        It "clones and archives a bare repo."
            make_bare_repo() {
                git init "${1}"
                touch "${1}/a"
                git -C "${1}" stage a
                git -C "${1}" commit -m "a"
                mv "${1}/.git" "${1}.git"
                rm -rf "${1}"
            }

            make_bare_repo git-fixture.git
            When call _kit capture repo "git-fixture.git"
            The file .kit/artifacts/git-fixture/repo.git.tar.bz2 should exist
        End
    End
End
