Describe "kit:"
    Include src/lib/_kit.bash

    It "displays a usage message"
        When call _kit
        The output should include "USAGE"
    End

End
