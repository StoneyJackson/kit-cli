FROM alpine:3 AS base
    ENTRYPOINT [ "kit" ]
    RUN apk add --update --no-cache \
            bash~=5 \
            git~=2 \
            github-cli~=2 \
            glab~=1 \
            curl~=8 \
            jq~=1 \
        && git config --global init.defaultBranch main \
        && git config --global user.name "kit" \
        && git config --global user.email "kit@example.com"
    COPY src/ /app/src/
    ENV PATH="$PATH:/app/src/bin"


FROM base AS test
    RUN git clone https://github.com/shellspec/shellspec.git /shellspec
    ENV PATH="$PATH:/shellspec"
    COPY spec /app/spec/
    COPY .shellspec /app/
    WORKDIR /kit


FROM base AS kit
    WORKDIR /kit
