SRC_LIB_DIR="$(cd -- "$(dirname -- "${BASH_SOURCE[0]:-$0}";)" &>/dev/null && pwd 2>/dev/null;)";

set -eu

source "${SRC_LIB_DIR}/import.bash"
import auth
import capture
import deploy
import help

_kit() {
    authorize_git_with_gitlab

    if [ $# -eq 0 ]; then
        help
    elif [ "$1" == "help" ]; then
        shift
        help "$@"
    elif [ "$1" == "capture" ] ; then
        shift
        capture "$@"
    elif [ "$1" == "deploy" ] ; then
        shift
        deploy "$@"
    fi
}
