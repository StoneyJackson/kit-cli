import git

capture() {
    local url
    local pathname

    local is_capture_repo
    local project_dir

    is_capture_repo=false
    if [ "$1" == repo ] ; then
        is_capture_repo=true
        shift
    fi

    url="$1"
    project_dir="$(get_project_dir "${url}" "${2:-}")"
    create_local_project "${project_dir}"

    if [ "$is_capture_repo" == true ] ; then
        capture_repo "${url}" "${project_dir}"
    fi
}

get_project_dir() {
    local url
    local pathname
    local project_dir
    url="${1}"
    pathname="$(get_pathname "${url}" "${2:-}")"
    project_dir="$(get_dotkit_dir "$PWD")/artifacts/${pathname}"
    echo "${project_dir}"
}


get_pathname() {
    local url
    local project
    local pathname
    url="$1"
    project="$(basename "${url}" .git)"
    pathname="${2:-${project}}"
    echo "${pathname}"
}

get_dotkit_dir() {
    cd "${1}" || exit 1
    while [ ! -d .kit ] && [ "${PWD}" != / ] ; do
        cd ..
    done
    if [ -d .kit ] ; then
        cd .kit || exit 1
        pwd
    else
        echo "./.kit"
    fi
}

create_local_project() {
    local project_dir
    project_dir="$1"
    mkdir -p "${project_dir}"
    touch "${project_dir}/project"
}

capture_repo() {
    local url
    local path
    url="$1"
    path="$2"
    git clone --bare -- "${url}" "${path}/repo.git"
    tar -jcf "${path}/repo.git.tar.bz2" --directory "${path}" repo.git
    rm -rf "${path}/repo.git"
}
